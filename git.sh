#!/bin/bash

#
# This shell script is for use with incron file monitoring to deploy from git when triggered to do so via the creation of git.flag
#
# $1 folder where the project lives
# $2 file change that triggered the build. Should only change when git.flag is created in the folder. incron is not capable of watching files that dont yet exist.
# $3 is the branch to deploy
#
if [ "$1" = "" ]; then
        echo "git.sh - This shell script is for use with incron file monitoring to deploy from git when triggered to do so"
        echo "git.sh <folder> <file> <branch>"
        exit
fi
if [ "$2" = git.flag ]; then
#cd to the folder where the git deployment lives
cd $1

        if [ -e "git.flag" ]; then
                slackecho "Deploying to $1"
                #remove flag
                rm git.flag

                #git fetch / reset deployment
                git fetch --all
                git reset --hard origin/$3

                #fix permissions
                chown www-data:www-data $1 -R
                slackecho "Completed"
        fi

fi
