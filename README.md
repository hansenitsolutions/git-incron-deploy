# README #

Set of scripts to automatically update from a remote git repository via a trigger.

This trigger can be an external git repository. I used [gitlab ](https://gitlab.com/gitlab-org/gitlab-ce) to access a URL on the development and production systems, keeping them up to date with the remote git repository as commits and merges happened.

### How do I get set up? ###

* clone your git repository as normal. you'll need to setup pre-authentication to enable automatic deployments.
* place git.sh on the filesystem, make it executable with chmod +x
* install incron (apt-get install incron)
* place example configuration file in /etc/incron.d/ and adjust with paths / branch
* setup a trigger. there is an example for php and shell in the examples folder. this needs to create a trigger that incron can monitor

### Who do I talk to? ###

* gary@hansenit.solutions
* [Hansen IT Solutions](https://hansenit.solutions)